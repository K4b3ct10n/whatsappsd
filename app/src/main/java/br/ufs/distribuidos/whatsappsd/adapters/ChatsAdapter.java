package br.ufs.distribuidos.whatsappsd.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import br.ufs.distribuidos.whatsappsd.R;
import br.ufs.distribuidos.whatsappsd.activities.ChatsActivity;
import br.ufs.distribuidos.whatsappsd.activities.ConversationActivity;
import br.ufs.distribuidos.whatsappsd.model.Chat;
import br.ufs.distribuidos.whatsappsd.model.Conversation;

/**
 * Created by Junior on 05/12/2015.
 */
public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ViewHolder> {

    private ArrayList<Chat> chats;
    private ChatsActivity activity;
    private static HashMap<String,Integer> receiversPosition;

    public ChatsAdapter(ArrayList<Chat> chats){
        this.chats = chats;
        receiversPosition = new HashMap<>();
    }

    public static HashMap<String,Integer> getReceiversPositionInstance(){
        return receiversPosition;
    }

    public int getReceiverPosition(String receiver){
        return receiversPosition.get(receiver) != null ? receiversPosition.get(receiver) : -1;
    }

    public void setReceiverPosition(String receiver, int position){
        receiversPosition.put(receiver,position);
    }

    public HashMap<String, Integer> getReceiversPosition() {
        return receiversPosition;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Chat chat = chats.get(position);
        if (receiversPosition.get(chat.getReceiver()) == null)
            receiversPosition.put(chat.getReceiver(),position);

        holder.receiver.setText(chat.getReceiver());
        holder.conversations = chat.getConversations();
        holder.position = position;
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public void setActivity(ChatsActivity activity) {
        this.activity = activity;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView receiver;
        private ArrayList<Conversation> conversations;
        private int position;

        public ViewHolder(View itemView) {
            super(itemView);
            this.receiver = (TextView) itemView.findViewById(R.id.receiver);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(activity, ConversationActivity.class);
            intent.putExtra("receiver", receiver.getText());
            intent.putExtra("position",position);
            activity.startActivity(intent);
        }
    }
}
