package br.ufs.distribuidos.whatsappsd.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.OnAccountsUpdateListener;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toolbar;

import java.util.ArrayList;

import br.ufs.distribuidos.whatsappsd.R;
import br.ufs.distribuidos.whatsappsd.adapters.ChatsAdapter;
import br.ufs.distribuidos.whatsappsd.controller.AppServiceController;
import br.ufs.distribuidos.whatsappsd.database.SQLiteHandler;
import br.ufs.distribuidos.whatsappsd.dialogs.AddFriendDialog;
import br.ufs.distribuidos.whatsappsd.interfaces.AddUser;
import br.ufs.distribuidos.whatsappsd.model.Chat;
import br.ufs.distribuidos.whatsappsd.services.NetworkService;
import br.ufs.distribuidos.whatsappsd.utils.DividerItemDecoration;

import static br.ufs.distribuidos.whatsappsd.utils.DividerItemDecoration.VERTICAL_LIST;

/**
 * The Class ChatsActivity is the Activity class. It shows a list of all users of
 * this app. It also shows the Offline/Online status of users.
 */
public class ChatsActivity extends CustomActivity implements AddUser {

    private static final String TAG = "ChatsActivity";
    protected static final String ACCOUNT_MANAGER_TYPE = "br.ufs.distribuidos.whatsd.user";

    private SQLiteHandler database;
    private ArrayList<Chat> chats;
    private ChatsAdapter adapter;
    private AccountManager accountManager;
    public static String user;

    private NetworkService service;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats_list);
        getActionBar().setDisplayHomeAsUpEnabled(false);

        // Account Manager
        accountManager = AccountManager.get(getApplicationContext());
        final Account[] accounts = accountManager.getAccountsByType(ACCOUNT_MANAGER_TYPE);

        // AccountManager listenner for logout
        // Listenner for logout
        OnAccountsUpdateListener accountsListener = new OnAccountsUpdateListener() {
            @Override
            public void onAccountsUpdated(Account[] accounts) {
                Account[] myAccounts = AccountManager.get(getApplicationContext()).getAccountsByType(ACCOUNT_MANAGER_TYPE);
                if (myAccounts.length == 0) {
                    // The account has been deleted
                    database.deleteUser();
                    chats.clear();

                    Intent i = new Intent(ChatsActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        };
        accountManager.addOnAccountsUpdatedListener(accountsListener, null, true);

        // Save login no Cache
        SharedPreferences settings = getSharedPreferences("user", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("login", accounts[0].name);
        editor.commit();

        service = AppServiceController.getAppServiceControllerInstance().getService();
        AppServiceController.getAppServiceControllerInstance().setActivity(this);
        // Adapter
        chats = AppServiceController.getChatsInstance();
        adapter = AppServiceController.getChatsAdapterInstance(this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), VERTICAL_LIST));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setSmoothScrollbarEnabled(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        // Database and data
        database = new SQLiteHandler(getApplicationContext());

        if (chats.size() == 0) {
            chats.addAll(database.getChats());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chats, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_add_pedido:
                DialogFragment newFragment = AddFriendDialog.newInstance();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.commit();
                newFragment.setTargetFragment(newFragment, 1);
                newFragment.show(getSupportFragmentManager(), "dialog");
                break;
            case R.id.action_exit:
                Account[] myAccounts = AccountManager.get(this).getAccountsByType(ACCOUNT_MANAGER_TYPE);
                accountManager.removeAccount(myAccounts[0], null, null);
                break;
        }
        return true;
    }

    @Override
    public void onFinishEditDialog(String user, EditText t) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(t.getWindowToken(), 0);
        database.addUser(user);
        Chat chat = new Chat(chats.size(),user);
        AppServiceController.getChatsAdapterInstance(this).setReceiverPosition(user,chat.getId());
        chats.add(chat);
        adapter.notifyDataSetChanged();
        if (service == null){
            service = AppServiceController.getAppServiceControllerInstance().getService();
        }
        service.startThread(chat);

    }
}
