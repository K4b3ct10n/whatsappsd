package br.ufs.distribuidos.whatsappsd.database;

/**
 * Created by pc on 03/12/2015.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import br.ufs.distribuidos.whatsappsd.adapters.ChatsAdapter;
import br.ufs.distribuidos.whatsappsd.model.Chat;
import br.ufs.distribuidos.whatsappsd.model.Conversation;

/**
 * Created by Junior on 06/07/2015.
 */
public class SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "whatsd";

    // LoginActivity table name
    private static final String TABLE_USERS = "users";
    private static final String TABLE_CHATS = "chats";

    // Users Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_USER = "user";

    // Chats Table Columns names
    // Uso o atributo KEY_ID declarado acima
    private static final String KEY_RECEIVER_ID = "receiver_id";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_DATE = "date";
    private static final String KEY_MSG_TYPE = "msg_type"; //0 - sent , 1 - received

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS + " ("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_USER + " TEXT )";

        String CREATE_CHATS_TABLE = "CREATE TABLE " + TABLE_CHATS + " ("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_RECEIVER_ID + " INTEGER, " + KEY_MESSAGE + " TEXT, "
                + KEY_DATE + " TEXT, " + KEY_MSG_TYPE + " INTEGER, " +
                "FOREIGN KEY(receiver_id) REFERENCES users(id)" + " )";
        db.execSQL(CREATE_USERS_TABLE);
        db.execSQL(CREATE_CHATS_TABLE);
        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHATS);

        // Create tables again
        onCreate(db);
    }

    public void addUser(String user) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Creating data storage
        ContentValues values = new ContentValues();
        values.put(KEY_USER, user);

        // Inserting Row
        long id = db.insert(TABLE_USERS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + user);
    }

    public int getUserId(String user){
        String query = "SELECT * " +
                "FROM users " +
                "WHERE user = '"+user+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            return cursor.getInt(0);
        }else{
            return -1;
        }
    }

    public void addConversation(String receiver, String date, String message, int msg_type) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Creating data storage
        ContentValues values = new ContentValues();
        values.put(KEY_RECEIVER_ID, getUserId(receiver));
        values.put(KEY_MESSAGE, message);
        values.put(KEY_DATE, date);
        values.put(KEY_MSG_TYPE, msg_type);

        // Inserting Row
        long id = db.insert(TABLE_CHATS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New message inserted into sqlite: Msg: " + message + " , Data: " + date +
                   " , Tipo: "+msg_type+ " , Destinatário: "+receiver);
    }

    public ArrayList<Chat> getChats(){
        SQLiteDatabase db = this.getReadableDatabase();
        SQLiteDatabase db2 = this.getReadableDatabase();
        ArrayList<Chat> chats = new ArrayList<>();
        String query = "SELECT * FROM users";
        Cursor cursor = db.rawQuery(query, null);
        Cursor cursor2 = null;
        if (cursor.moveToFirst()) {
            do {
                String query2 = "SELECT * " +
                        "FROM chats JOIN users ON " +
                        "chats.receiver_id = users.id " +
                        "WHERE users.id = "+cursor.getInt(0);
                cursor2 = db2.rawQuery(query2, null);
                if (cursor2.moveToFirst()) {
                    ArrayList<Conversation> conversations = new ArrayList<>();
                    conversations.add(new Conversation(cursor.getInt(0),cursor2.getString(2),cursor2.getString(3), cursor2.getInt(4) == 1));
                    while(cursor2.moveToNext()) {
                        conversations.add(new Conversation(cursor.getInt(0),cursor2.getString(2),cursor2.getString(3), cursor2.getInt(4) == 1));
                    }
                    ChatsAdapter.getReceiversPositionInstance().put(cursor.getString(1),chats.size());
                    chats.add(new Chat(chats.size(), cursor.getInt(0), cursor.getString(1), conversations));
                }else{
                    ChatsAdapter.getReceiversPositionInstance().put(cursor.getString(1),chats.size());
                    chats.add(new Chat(chats.size(), cursor.getInt(0), cursor.getString(1)));
                }
            }while (cursor.moveToNext());
            cursor2.close();
        }else{
            cursor.close();
            return chats;
        }
        cursor.close();
        return chats;
    }

    public void deleteUser() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USERS, null, null);
        db.delete(TABLE_CHATS, null, null);
        db.close();
        Log.d(TAG, "Deleted all user info from sqlite");
    }
}