package br.ufs.distribuidos.whatsappsd.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.EditText;
import android.widget.Toast;

import br.ufs.distribuidos.whatsappsd.activities.ChatsActivity;

/**
 * Created by Junior on 27/08/2015.
 */
public class AddFriendDialog extends DialogFragment {

    private static final String TAG = "AddFriendDialog";
    public static AddFriendDialog newInstance() {
        return new AddFriendDialog();
    }

    public AddFriendDialog(){
        setArguments(getArguments());
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final EditText t = (EditText) new EditText(getContext());
        t.setHint("Nome");
        // Use the Builder class for convenient dialog construction
        return new  AlertDialog.Builder(getActivity())
                .setView(t)
                .setMessage("Digite o nome do seu amigo: ")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (t.getText().toString().equals("")) {
                            Toast.makeText(getContext(),"Nome de usuário inválido",Toast.LENGTH_LONG).show();
                            return;
                        }
                        ChatsActivity activity = (ChatsActivity) getActivity();
                        activity.onFinishEditDialog(t.getText().toString(), t);
                        dismiss();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                })
                .create();
    }
}
