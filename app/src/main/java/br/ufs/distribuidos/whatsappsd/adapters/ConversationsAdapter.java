package br.ufs.distribuidos.whatsappsd.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import br.ufs.distribuidos.whatsappsd.R;
import br.ufs.distribuidos.whatsappsd.activities.ConversationActivity;
import br.ufs.distribuidos.whatsappsd.model.Chat;
import br.ufs.distribuidos.whatsappsd.model.Conversation;

/**
 * Created by Junior on 05/12/2015.
 */
public class ConversationsAdapter extends RecyclerView.Adapter<ConversationsAdapter.ViewHolder> {

    private static final String TAG = "ConversationsAdapter";
    private ArrayList<Chat> chats;
    private int receiverPosition;
    private ConversationActivity activity;

    public ConversationsAdapter(ArrayList<Chat> chats){
        this.chats = chats;
    }

    public ConversationActivity getActivity() {
        return activity;
    }

    public void setActivity(ConversationActivity activity){
        this.activity = activity;
    }

    public int getReceiverPosition() {
        return receiverPosition;
    }

    public void setReceiverPosition(int receiverPosition) {
        this.receiverPosition = receiverPosition;
    }

    @Override
    public int getItemViewType(int position) {
        return chats.get(this.receiverPosition).getConversations().get(position).isReceived() ? 1 : 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch(viewType){
            case 0: // Mensagem enviada
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_sent, parent, false);
                break;
            case 1: // Mensagem recebida
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_rcv, parent, false);
                break;
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Conversation conversation = chats.get(this.receiverPosition).getConversations().get(position);

        holder.time.setText(conversation.getDate());
        holder.message.setText(conversation.getMsg());
    }

    @Override
    public int getItemCount() {
        return chats.get(this.receiverPosition).getConversations().size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView time;
        private TextView message;
        private TextView status;

        public ViewHolder(View itemView) {
            super(itemView);
            this.time = (TextView) itemView.findViewById(R.id.lbl1);
            this.message = (TextView) itemView.findViewById(R.id.lbl2);
            this.status = (TextView) itemView.findViewById(R.id.lbl3);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            /*Intent intent = new Intent(activity, ConversationActivity.class);
            intent.putExtra("conversations",conversations);
            activity.startActivity(intent);
            activity.finish();*/
        }
    }
}
