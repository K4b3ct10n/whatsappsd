package br.ufs.distribuidos.whatsappsd.interfaces;

import android.widget.EditText;

/**
 * Created by Junior on 09/12/2015.
 */
public interface AddUser {
    void onFinishEditDialog(String user, EditText t);
}
