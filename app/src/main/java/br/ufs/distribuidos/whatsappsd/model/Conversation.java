package br.ufs.distribuidos.whatsappsd.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Junior on 03/12/2015.
 * The Class Conversation is a Java Bean class that represents a single activity_conversation
 * conversation message.
 */
public class Conversation implements Parcelable{

    /** The Constant STATUS_SENDING. */
    public static final int STATUS_SENDING = 0;

    /** The Constant STATUS_SENT. */
    public static final int STATUS_SENT = 1;

    /** The Constant STATUS_FAILED. */
    public static final int STATUS_FAILED = 2;

    private int id;
    private String msg;
    private String date;
    private boolean received; /** identifica se a mensagem foi enviada ou recebida pelo user */
    // TODO private int status = STATUS_SENT; /** The status. */

    public Conversation(int id, String msg, String date, boolean received) {
        this.id = id;
        this.msg = msg;
        this.date = date;
        this.received = received;
    }

    public Conversation(String msg, String date, boolean received) {
        this.msg = msg;
        this.date = date;
        this.received = received;
    }

    protected Conversation(Parcel in) {
        id = in.readInt();
        msg = in.readString();
        date = in.readString();
        received = (boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Creator<Conversation> CREATOR = new Creator<Conversation>() {
        @Override
        public Conversation createFromParcel(Parcel in) {
            return new Conversation(in);
        }

        @Override
        public Conversation[] newArray(int size) {
            return new Conversation[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isReceived() {
        return received;
    }

    public void setReceived(boolean received) {
        this.received = received;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(msg);
        dest.writeString(date);
        dest.writeValue(received);
    }
}
