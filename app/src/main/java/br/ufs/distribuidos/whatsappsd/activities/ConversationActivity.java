package br.ufs.distribuidos.whatsappsd.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import br.ufs.distribuidos.whatsappsd.R;
import br.ufs.distribuidos.whatsappsd.adapters.ConversationsAdapter;
import br.ufs.distribuidos.whatsappsd.controller.AppServiceController;
import br.ufs.distribuidos.whatsappsd.database.SQLiteHandler;
import br.ufs.distribuidos.whatsappsd.model.Chat;
import br.ufs.distribuidos.whatsappsd.model.Conversation;
import br.ufs.distribuidos.whatsappsd.services.NetworkService;

/**
 * The Class ConversationActivity is the Activity class that holds main activity_conversation screen. It shows
 * all the conversation messages between two users and also allows the user to
 * send and receive messages.
 */
public class ConversationActivity extends CustomActivity {

    public RecyclerView recyclerView;
    private ArrayList<Chat> chats;
    private ConversationsAdapter adapter;
    private EditText txtMessage;

    private String receiver;

    private int position; // Position on the chat list

    private NetworkService service;
    private SQLiteHandler database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        getActionBar().setTitle(getIntent().getStringExtra("receiver"));

        service = AppServiceController.getAppServiceControllerInstance().getService();
        receiver = getIntent().getStringExtra("receiver");
        database = new SQLiteHandler(getApplicationContext());

        position = getIntent().getIntExtra("position", 0);
        chats = AppServiceController.getChatsInstance();
        adapter = AppServiceController.getConversationsAdapterInstance(position);
        adapter.setActivity(this);
        recyclerView = (RecyclerView) findViewById(R.id.list_conversations);
        LinearLayoutManager llmanager = new  LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llmanager);
        recyclerView.setAdapter(adapter);
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);

        txtMessage = (EditText) findViewById(R.id.txt);
        txtMessage.setInputType(InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_FLAG_MULTI_LINE);

        setTouchNClick(R.id.btnSend);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == R.id.btnSend){
            sendMessage();
        }
    }

    private void sendMessage(){
        if (txtMessage.length() == 0)
            return;
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txtMessage.getWindowToken(), 0);
        Conversation c = new Conversation(chats.get(position).getConversations().size(),txtMessage.getText().toString(),
                DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(new Date()),false);
        service.sendMessage(getSharedPreferences("user", 0).getString("login","junior"),receiver, c);
        database.addConversation(receiver,c.getDate(),c.getMsg(),0);
        chats.get(position).getConversations().add(c);
        adapter.notifyDataSetChanged();
        txtMessage.setText(null);
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);
    }
}
