package br.ufs.distribuidos.whatsappsd.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import br.ufs.distribuidos.whatsappsd.R;


/**
 * The Class LoginActivity is an Activity class that shows the activity_login screen to users.
 * The current implementation simply includes the options for LoginActivity and button
 * for RegisterActivity. On activity_login button click, it sends the LoginActivity details to Parse
 * server to verify user.
 */
public class LoginActivity extends CustomActivity {

    /** Usuário */
    private EditText user;
    private AccountManager accountManager;
    protected static final String ACCOUNT_MANAGER_TYPE = "br.ufs.distribuidos.whatsd.user";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Account Manager
        accountManager = AccountManager.get(getApplicationContext());
        final Account[] accounts = accountManager.getAccountsByType(ACCOUNT_MANAGER_TYPE);

        // Checa se usuário está logado
        if (accounts.length != 0) {
            Intent intent = new Intent(LoginActivity.this, ChatsActivity.class);
            startActivity(intent);
            finish();
        }

        setTouchNClick(R.id.btnLogin);

        user = (EditText) findViewById(R.id.user);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (user.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Preencha o campo com o nome de usuário",Toast.LENGTH_SHORT).show();
            return;
        }

        Account account = new Account(user.getText().toString(),ACCOUNT_MANAGER_TYPE);
        accountManager.addAccountExplicitly(account,null,null);

        // Save login no Cache
        SharedPreferences settings = getSharedPreferences("user", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("login", user.getText().toString());
        editor.commit();

        startActivity(new Intent(LoginActivity.this, ChatsActivity.class));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10 && resultCode == RESULT_OK)
            finish();

    }
}
