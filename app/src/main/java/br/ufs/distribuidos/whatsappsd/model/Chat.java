package br.ufs.distribuidos.whatsappsd.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.rabbitmq.client.Channel;

import java.util.ArrayList;

/**
 * Created by Junior on 03/12/2015.
 */
public class Chat implements Parcelable {
    private int id;
    private int receiver_id;
    private String receiver;
    private ArrayList<Conversation> conversations;
    private Channel channel;

    public Chat(int id, String receiver){
        this.id = id;
        this.receiver = receiver;
        this.conversations = new ArrayList<>();
    }

    public Chat(int id, int id_receiver, String destinatario, ArrayList<Conversation> conversas, Channel channel) {
        this.id = id;
        this.receiver_id = id_receiver;
        this.receiver = destinatario;
        this.conversations = conversas;
        this.channel = channel;
    }

    public Chat(int id, int id_receiver, String destinatario){
        this.id = id;
        this.receiver_id = id_receiver;
        this.receiver = destinatario;
        this.conversations = new ArrayList<>();
    }

    public Chat(int id, int id_receiver, String destinatario, ArrayList<Conversation> conversas){
        this.id = id;
        this.receiver_id = id_receiver;
        this.receiver = destinatario;
        this.conversations = conversas;
    }

    protected Chat(Parcel in) {
        id = in.readInt();
        receiver_id = in.readInt();
        receiver = in.readString();
        conversations = new ArrayList<>();
        in.readList(conversations,Chat.class.getClassLoader());
    }

    public static final Creator<Chat> CREATOR = new Creator<Chat>() {
        @Override
        public Chat createFromParcel(Parcel in) {
            return new Chat(in);
        }

        @Override
        public Chat[] newArray(int size) {
            return new Chat[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReceiverId() {
        return receiver_id;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public ArrayList<Conversation> getConversations() {
        return conversations;
    }

    public void addConversation(Conversation conversa){
        this.conversations.add(conversa);
    }

    public void addConversations(ArrayList<Conversation> conversas){
        this.conversations.addAll(conversas);
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(receiver_id);
        dest.writeString(receiver);
        dest.writeList(conversations);
    }
}
