package br.ufs.distribuidos.whatsappsd.controller;

import android.app.Activity;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.UiThread;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import java.io.IOException;
import java.util.ArrayList;

import br.ufs.distribuidos.whatsappsd.R;
import br.ufs.distribuidos.whatsappsd.activities.ChatsActivity;
import br.ufs.distribuidos.whatsappsd.activities.LoginActivity;
import br.ufs.distribuidos.whatsappsd.adapters.ChatsAdapter;
import br.ufs.distribuidos.whatsappsd.adapters.ConversationsAdapter;
import br.ufs.distribuidos.whatsappsd.database.SQLiteHandler;
import br.ufs.distribuidos.whatsappsd.model.Chat;
import br.ufs.distribuidos.whatsappsd.model.Conversation;
import br.ufs.distribuidos.whatsappsd.services.NetworkService;

/**
 * Created by Junior on 08/12/2015.
 */
public class AppServiceController extends Application {

    private static AppServiceController mInstance;

    private static ArrayList<Chat> chats;
    private static ChatsAdapter chatsAdapter;
    private static ArrayList<Conversation> conversations;
    private static ConversationsAdapter convsAdapter;

    private NetworkService service;
    private SQLiteHandler database;
    private Activity activity;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public static synchronized AppServiceController getAppServiceControllerInstance() {
        return mInstance;
    }

    public static synchronized ChatsAdapter getChatsAdapterInstance(ChatsActivity chatsActivity) {
        chatsAdapter.setActivity(chatsActivity);
        return chatsAdapter;
    }
    public static synchronized ArrayList<Chat>  getChatsInstance() {
        return chats;
    }

    public static synchronized ConversationsAdapter getConversationsAdapterInstance(int position) {
        convsAdapter.setReceiverPosition(position);
        return convsAdapter;
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,IBinder binder) {
            try {
                NetworkService.MyBinder b = (NetworkService.MyBinder) binder;
                service = b.getService();
                service.startThreads(chats);
                Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_SHORT).show();
            }catch(ClassCastException e){

            }
        }

        public void onServiceDisconnected(ComponentName className) {
            service = null;
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        chats = new ArrayList<>();
        chatsAdapter = new ChatsAdapter(chats);
        conversations = new ArrayList<>();
        convsAdapter = new ConversationsAdapter(chats);

        database = new SQLiteHandler(getApplicationContext());
        Intent intent = new Intent(this,NetworkService.class);
        intent.putExtra("chats", database.getChats());
        bindService(intent, mConnection, BIND_AUTO_CREATE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (service == null && activity == null){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                // Setup consumer
                Consumer consumer = new DefaultConsumer(service.getReceiverChannel()){
                    @Override
                    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                            throws IOException {
                        String message = new String(body, "UTF-8");
                        final String[] msg = message.split("\n");
                        if (chatsAdapter.getReceiverPosition(msg[0]) == -1){ // Caso usuário novo mande mensagem, e não esteja na lista de "amigos"
                            Chat c = new Chat(chats.size(),msg[0]);
                            chats.add(c);
                            chatsAdapter.setReceiverPosition(msg[0], c.getId());
                            service.startThread(c);
                            database.addUser(msg[0]);
                        }
                        chats.get(chatsAdapter.getReceiverPosition(msg[0])).addConversation(new Conversation(msg[2], msg[1], true));
                        database.addConversation(msg[0], msg[1], msg[2], 1);
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            @UiThread
                            public void run() {
                                chatsAdapter.notifyDataSetChanged();
                                convsAdapter.notifyDataSetChanged();
                                if (convsAdapter.getActivity() != null)
                                    convsAdapter.getActivity().recyclerView.scrollToPosition(convsAdapter.getItemCount() - 1);

                                // Cria uma notificação na barra de notificações
                                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext());
                                notification.setSmallIcon(R.drawable.icon);
                                notification.setContentTitle("WhatSD");
                                notification.setContentText("Nova mensagem de " + msg[0]);

                                // Abre activity ao tocar na notificação
                                Intent notificationIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0,
                                        notificationIntent, 0);
                                notification.setContentIntent(intent);
                                notification.setAutoCancel(true);

                                // Configura alertas de som, luz e vibração
                                notification.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS);
                                long[] pattern = {500,0,0,500};
                                notification.setVibrate(pattern);

                                // Notifica
                                notificationManager.notify(0, notification.build());
                            }
                        });
                    }
                };
                service.setReceiverConsumer(consumer);
            }
        }).start();
    }

    public NetworkService getService(){
        return service;
    }
}
