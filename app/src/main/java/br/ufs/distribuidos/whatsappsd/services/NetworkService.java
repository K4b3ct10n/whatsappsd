package br.ufs.distribuidos.whatsappsd.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeoutException;

import br.ufs.distribuidos.whatsappsd.model.Chat;
import br.ufs.distribuidos.whatsappsd.model.Conversation;

/**
 * Created by Junior on 05/12/2015.
 */
public class NetworkService extends Service {

    private static final String TAG = "NetworkService";
    private static ConnectionFactory factory;
    private static Connection connection;
    private HashMap<String,Channel> canais;
    private final IBinder mBinder = new MyBinder();
    private ThreadPerTaskExecutor threads;
    private Channel receiverChannel;
    private Consumer receiverConsumer;

    public Channel getReceiverChannel() {
        while(receiverChannel == null){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Log.e(TAG, "Erro: " + e.getMessage());
            }
        }
        return receiverChannel;
    }

    public void setReceiverConsumer(Consumer receiverConsumer) {
        this.receiverConsumer = receiverConsumer;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //android.os.Debug.waitForDebugger(); Comentar sempre que for executar
        if (factory == null){
            factory = new ConnectionFactory();
            factory.setHost("k4b3ct10n.koding.io");
            factory.setUsername("guest");
            factory.setPassword("guest");
            try {
                ExecutorService executor = Executors.newSingleThreadExecutor();
                FutureTask future =
                        (FutureTask) new FutureTask(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    connection = factory.newConnection();
                                    receiverChannel = connection.createChannel();
                                } catch (IOException | TimeoutException e) {
                                    Log.e(TAG, "Erro de conexão: " + e.getMessage());
                                }
                            }
                        },null);
                executor.execute(future);
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                Log.e(TAG, "Erro de conexão: " + e.getMessage());
            }
            canais = new HashMap<>();
        }
    }

    public void restartConnection(){
        try {
            connection.close(); // Encerra conexão antiga
            ExecutorService executor = Executors.newSingleThreadExecutor();
            FutureTask future =
                    (FutureTask) new FutureTask(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                connection = factory.newConnection();
                                receiverChannel = connection.createChannel();
                            } catch (IOException | TimeoutException e) {
                                Log.e(TAG, "Erro de conexão: " + e.getMessage());
                            }
                        }
                    },null);
            executor.execute(future);
            future.get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(TAG, "Erro de conexão: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "Erro de encerrando conexão: " + e.getMessage());
        }
        canais = new HashMap<>();
    }

    public ConnectionFactory getConnectionFactory() {
        return factory;
    }

    public static Connection getConnection() {
        return connection;
    }

    public void startThreads(ArrayList<Chat> chats){
        threads = new ThreadPerTaskExecutor();
        for (Chat c: chats) {
            threads.openChannels(c.getReceiver(), canais);
        }
        threads.openReceiver();
    }

    public void startThread(Chat chat){
        if (threads != null){
            threads.openChannels(chat.getReceiver(),canais);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    private String codificarMensagem(String sender, Conversation conversation) {
        StringBuilder sb = new StringBuilder();
        sb.append(sender);
        sb.append("\n");
        sb.append(conversation.getDate());
        sb.append("\n");
        sb.append(conversation.getMsg());
        return sb.substring(0);
    }

    public void sendMessage(String sender, String receiver, Conversation conversation){
        threads.sendMessage(receiver, codificarMensagem(sender,conversation));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            connection.close();
        } catch (IOException e) {
            Log.e(TAG, "Erro encerrando a conexão: " + e.getMessage());
        }
    }

    /**
     * Classe que trata os canais pra cada fila de mensagens, criando threads pra cada um deles
     */
    class ThreadPerTaskExecutor implements Executor {

        protected void openChannels(final String receiver, final HashMap<String, Channel> canais){
            execute(new Runnable() {
                @Override
                public void run() {
                    Channel channel = null;
                    try {
                        channel = connection.createChannel();
                        canais.put(receiver,channel);
                    } catch (IOException e) {
                        Log.e(TAG, "Erro de conexão: " + e.getMessage());
                    }
                }
            });
        }

        protected void openReceiver(){
            execute(new Runnable() {
                @Override
                public void run() {
                    while (receiverConsumer == null){
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    Looper.prepare();

                    try {
                        String receiver = getSharedPreferences("user",0).getString("login","junior");
                        receiverChannel.queueDeclare(receiver, false, false, false, null);
                        receiverChannel.basicConsume(receiver, true, receiverConsumer);
                    } catch (IOException e) {
                        Log.e(TAG, "Erro nas Preferências Compartilhadas: " + e.getMessage());
                    }

                    Looper.loop();
                }
            });
        }

        protected void sendMessage(final String receiver, final String msg){
            execute(new Runnable() {
                @Override
                public void run() {
                    Looper.prepare();

                    try {
                        canais.get(receiver).queueDeclare(receiver, false, false, false, null); // Se a fila não existe, será criada
                        canais.get(receiver).basicPublish("", receiver, null, msg.getBytes("UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        Log.e(TAG,"Erro de codificação da mensagem: "+e.getMessage());
                    } catch (IOException e) {
                        Log.e(TAG,"Erro enviando mensagem: "+e.getMessage());
                    }

                    Looper.loop();
                }
            });
        }

        public void execute(Runnable r) {
            new Thread(r).start();
        }}

    public class MyBinder extends Binder {
        public NetworkService getService() {
            return NetworkService.this;
        }
    }
}
